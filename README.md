# GitLab Tools

## Generate Repo List

Set environment variables for both GitLab Cloud and GitLab On-prem.

```sh
export GL_PAT=pat_of_gitlab_cloud
export DCGL_PAT=pat_of_gitlab_onprem
```

Install required python library.

```sh
pip install --upgrade python-gitlab
```

Generate GitLab Cloud project list and GitLab on-prem group list.

```sh
PYTHONWARNINGS="ignore:Unverified HTTPS request" ./ybn_repos.py group_id_of_gitlab_cloud group_id_of_gitlab_onprem
```

Update new GitLab Group IDs.

```sh
./id_map.sh gl_project_list.csv
```

**Notes**

`group_id_of_gitlab_cloud` and `group_id_of_gitlab_onprem` might be different but group name should be the same.

## Create Projects

Create the projects on GitLab on-prem.

```sh
PYTHONWARNINGS="ignore:Unverified HTTPS request" ./ybn_create_repos.py gl_project_list.csv
```

