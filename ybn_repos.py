#!/usr/bin/env python3
import os, gitlab, csv, sys

gl_pat = os.environ['GL_PAT']
gl = gitlab.Gitlab(oauth_token=gl_pat)
group_id = sys.argv[1]

dcgl_pat = os.environ['DCGL_PAT']
#dcgl = gitlab.Gitlab(url='https://onprem-gitlab.yomabank.io', oauth_token=dcgl_pat, ssl_verify=False)
dcgl = gitlab.Gitlab(url='https://onprem-gitlab.yomabank.io', oauth_token=dcgl_pat)
#dc_group_id = '8'
dc_group_id = sys.argv[2]

def dc_group_list(dc_grp_id):
    group = dcgl.groups.get(dc_grp_id)
    subgrps = group.subgroups.list(all=True)
    for subgrp in subgrps:
        g = dcgl.groups.get(subgrp.id)
        dc_grp_list.append([g.id,g.path])
        dc_group_list(subgrp.id)

dc_grp_list = []
dc_group_list(dc_group_id)

with open('dcgl_group_list.csv', 'w') as f:
    writer = csv.writer(f, lineterminator="\n")
    writer.writerows(dc_grp_list)

def project_list(grp_id):
    group = gl.groups.get(grp_id)
    subgrps = group.subgroups.list(all=True)
    for subgrp in subgrps:
        g = gl.groups.get(subgrp.id)
        for p in g.projects.list(all=True):
            print(p.namespace['path'], ',', p.path, p.http_url_to_repo)
            proj_list.append([p.namespace['path'],p.name,p.path,p.http_url_to_repo])
        project_list(subgrp.id)

proj_list = []
project_list(group_id)

with open('gl_project_list.csv', 'w') as f:
    writer = csv.writer(f, lineterminator="\n")
    writer.writerows(proj_list)

#project = gl.projects.create({'name': 'myrepo', 'namespace_id': group_id})

