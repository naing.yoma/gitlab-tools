#!/bin/bash

mirror_path=/opt/gl_mirrors

for proj in $(ls splitted/proj_list*); do
  echo "$proj started at $(date)" | tee -a process_tracking.log
  while IFS=, read -r grp_id grp_name proj_name proj_path proj_url; do
    git_path=${proj_url#*gitlab.com/}
    cd ${mirror_path}/${git_path%/*}/${proj_path}.git
    echo "git push --mirror ${proj_url/gitlab.com/gitlab.yomabank.org}"
    git push --mirror ${proj_url/gitlab.com/gitlab.yomabank.org} 2>> ../error.log
    sleep 1
  done < $proj
  echo "$proj ended at $(date)" | tee -a process_tracking.log
  sleep 10
done
