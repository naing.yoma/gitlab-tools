#!/usr/bin/env python3
import os, gitlab, csv

gl_pat = os.environ['GL_PAT']
gl = gitlab.Gitlab(oauth_token=gl_pat)
group_id = 2377064

def project_list(grp_id):
    group = gl.groups.get(grp_id)
    subgrps = group.subgroups.list(all=True)
    for subgrp in subgrps:
        g = gl.groups.get(subgrp.id)
        for p in g.projects.list(all=True):
            print(p.namespace['path'], ',', p.path, p.http_url_to_repo)
            proj_list.append([p.namespace['path'],p.name,p.path,p.http_url_to_repo])
        project_list(subgrp.id)

proj_list = []
project_list(group_id)

with open('gl_project_list.csv', 'w') as f:
    writer = csv.writer(f, lineterminator="\n")
    writer.writerows(proj_list)

