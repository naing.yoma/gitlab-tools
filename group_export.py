#!/usr/bin/env python3
import os, gitlab, time

gl_pat = os.environ['GL_PAT']
gl = gitlab.Gitlab(oauth_token=gl_pat)
group_id = 2377064

def group_export(grp_id):
    print("Exporting Group: " + str(grp_id))
    group = gl.groups.get(grp_id)
    export = group.exports.create()
    time.sleep(60)

    with open('export.tgz', 'wb') as f:
        export.download(streamed=True, action=f.write)

group_export(group_id)
