#!/bin/bash

source /home/naingyeminn/.env

cp gl_project_list.csv gl_project_list.csv.bak

while IFS=, read -r grp_id grp_name; do
  sed -i "s/^$grp_name,/$grp_id,$grp_name,/g" gl_project_list.csv
done < dcgl_group_list.csv

grep -vf gl_project_list_last.csv gl_project_list.csv > gl_project_list_new.csv

cp gl_project_list_last.csv gl_project_list_$(date +%F).csv

cp gl_project_list.csv gl_project_list_last.csv

split -l 100 gl_project_list.csv splited/proj_list_

#./ybn_create_repos.py gl_project_list_new.csv
