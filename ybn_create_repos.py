#!/usr/bin/env python3
import os, gitlab, csv, sys

dcgl_pat = os.environ['DCGL_PAT']
dcgl = gitlab.Gitlab(url='https://onprem-gitlab.yomabank.io', oauth_token=dcgl_pat)


with open(sys.argv[1]) as f:
    csv_reader = csv.reader(f, delimiter=',')
    for row in csv_reader:
        print(f'create {row[2]} in group {row[0]}')
        try:
            project = dcgl.projects.create({'name': row[2], 'path': row[3], 'namespace_id': row[0]})
        except:
            print(f"{row[2]} has been created.")

