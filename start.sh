#!/bin/bash

split -l 100 gl_project_list.csv splitted/proj_list_
mirror_path=/opt/gl_mirrors

for proj in $(ls splitted/proj_list*); do
  echo "$proj started at $(date)" | tee -a ${mirror_path}/process_tracking.log
  while IFS=, read -r grp_name proj_name proj_path proj_url; do
    git_path=${proj_url#*gitlab.com/}
    echo "${git_path%/*}"
    mkdir -vp ${mirror_path}/${git_path%/*}
    cd ${mirror_path}/${git_path%/*}
    echo "git clone --mirror ${proj_url}"
    git clone --mirror ${proj_url} 2>> ${mirror_path}/clone_error.log
    if [ $? == 128 ]; then
      cd ${mirror_path}/${git_path%/*}/${proj_path}.git
      echo "Updating ${proj_path}.git"
      git remote update
    fi
  done < $proj
  cd ${mirror_path}
  echo "$proj ended at $(date)" | tee -a ${mirror_path}/process_tracking.log
  sleep 1800
done
